package com.gemy.news;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * Created by mada4 on 3/6/2018.
 */

public final class QueryUtils {

    private QueryUtils() {

    }


    public static ArrayList<NewsObject> extractNewsObjects(String requestUrl) throws IOException {
        Log.v("tag", "extract called");
        URL url = createUrl(requestUrl);

        String jsonResponse = null;

        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.v("tag", "json error");
        }
        ArrayList<NewsObject> newsObjects = new ArrayList<>();

        try {
            JSONObject rootJsonObject = new JSONObject(jsonResponse);
            JSONObject responseObj = rootJsonObject.getJSONObject("response");

            JSONArray resultsArray = responseObj.getJSONArray("results");
            for (int i = 0; i < resultsArray.length(); i++) {
                JSONObject newsObject = resultsArray.getJSONObject(i);
                String header = newsObject.getString("webTitle");
                String type = newsObject.getString("type");
                String sectionName = newsObject.getString("sectionName");
                JSONObject fields = newsObject.getJSONObject("fields");
                String imageUrl = fields.getString("thumbnail");
                URL obtainedUrl = new URL(imageUrl);
                Bitmap bitmap = BitmapFactory.decodeStream(obtainedUrl.openConnection().getInputStream());
                String webSiteUrl = newsObject.getString("webUrl");

                newsObjects.add(new NewsObject(header, bitmap, type, sectionName,webSiteUrl));

            }
        } catch (JSONException e) {
            Log.v("tag", "something with the internet");
        }

        Log.v("logTag", newsObjects.size() + "");

        return newsObjects;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        // If the URL is null, then return early.
        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            String code = urlConnection.getResponseCode() + "";
            Log.v("response code armadello", code);

            // If the request was successful (response code 200),
            // then read the input stream and parse the response.
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e("lol", "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e("lol", "Problem retrieving the earthquake JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            return null;
        }
        return url;
    }
}
