package com.gemy.news;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.List;

/**
 * Created by mada4 on 3/6/2018.
 */

public class NewsObjectsLoader extends AsyncTaskLoader<List<NewsObject>> {

    List<NewsObject> newsObjects;
    String usedUrl;

    public NewsObjectsLoader(Context context, String url) {
        super(context);
        usedUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<NewsObject> loadInBackground() {
        Log.v("TAG", "loading in background called");

        try {
            newsObjects = QueryUtils.extractNewsObjects(usedUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return newsObjects;
    }
}
