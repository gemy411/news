package com.gemy.news;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

public class NewsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<NewsObject>> {

    private static final String URLNews =
            "http://content.guardianapis.com/search?api-key=test&show-fields=thumbnail";

    ListView listView;
    NewsObjectsAdapter newsObjectsAdapter;
    LoaderManager loaderManager;
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        bind();
        loaderManager.initLoader(1, null, this);


    }

    private void bind() {
        loaderManager = getLoaderManager();
        listView = findViewById(R.id.list_view);
        progressBar = findViewById(R.id.progress_bar);


    }

    private void updateUi(List<NewsObject> news) {
        newsObjectsAdapter = new NewsObjectsAdapter(this, (ArrayList<NewsObject>) news);
        listView.setAdapter(newsObjectsAdapter);
        listItemClicks(news);

    }

    private void listItemClicks(final List<NewsObject> newsObjectsForClicks) {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String url = newsObjectsForClicks.get(i).getWebSiteUrl();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);

            }
        });
    }


    @Override
    public Loader<List<NewsObject>> onCreateLoader(int i, Bundle bundle) {
        Log.v("TAG", "creatloader called");
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String topics = sharedPrefs.getString(
                getString(R.string.settings_topic_key),
                getString(R.string.settings_topic_default));
        String orderBy = sharedPrefs.getString(
                getString(R.string.settings_order_by_key),
                getString(R.string.settings_order_by_default)
        );
        Uri baseUre = Uri.parse(URLNews);
        Uri.Builder builder = baseUre.buildUpon();

        builder.appendQueryParameter("section", topics);
        builder.appendQueryParameter("order-by", orderBy);

        return new NewsObjectsLoader(this, builder.toString());
    }

    @Override
    public void onLoadFinished(Loader<List<NewsObject>> loader, List<NewsObject> newsObjects) {
        progressBar.setVisibility(View.GONE);
        updateUi(newsObjects);

    }

    @Override
    public void onLoaderReset(Loader<List<NewsObject>> loader) {
        newsObjectsAdapter.clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
