package com.gemy.news;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by mada4 on 3/6/2018.
 */

public class NewsObjectsAdapter extends ArrayAdapter<NewsObject> {


    public NewsObjectsAdapter(@NonNull Context context, ArrayList<NewsObject> newsObjects) {
        super(context, R.layout.list_template, newsObjects);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View customView = layoutInflater.inflate(R.layout.list_template, parent, false);

        NewsObject newsObject = getItem(position);

        String header = newsObject.getHeader();
        TextView headerTV = customView.findViewById(R.id.header);
        headerTV.setText(header);

        Bitmap image = newsObject.getImage();
        ImageView imageView = customView.findViewById(R.id.item_image);
        ImageView imageViewBlurred = customView.findViewById(R.id.blurred_image);
        imageView.setImageBitmap(image);
        imageViewBlurred.setImageBitmap(image);

        String type = newsObject.getType();
        TextView typeTV = customView.findViewById(R.id.type);
        typeTV.setText(type);

        String sectionName = newsObject.getSectionName();
        TextView sectionNameTV = customView.findViewById(R.id.section_name);
        sectionNameTV.setText(sectionName);


        return customView;
    }

}
