package com.gemy.news;

import android.graphics.Bitmap;

/**
 * Created by mada4 on 3/6/2018.
 */

public class NewsObject {

    private String header;
    private Bitmap image;
    private String type;
    private String sectionName;
    private String webSiteUrl;



    public NewsObject(String consHeader, Bitmap consImage, String consType, String consSectionName, String consUrl) {
        header = consHeader;
        image = consImage;
        type = consType;
        sectionName = consSectionName;
        webSiteUrl = consUrl;


    }

    public String getWebSiteUrl() {
        return webSiteUrl;
    }

    public String getHeader() {
        return header;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getType() {
        return type;
    }

    public String getSectionName() {
        return sectionName;
    }


}
